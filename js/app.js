/*********************************************************************************************************************/
/*                                                      V1                                                           */
/*********************************************************************************************************************/

// let screen = document.querySelector(".screen-result p")
//
// let numberTable = document.querySelectorAll(".number button")
// let characterTable = document.querySelectorAll(".character button")
// let dotCharacter = document.querySelector(".dot button")
// let egal = document.querySelector(".button-l button")
// let reset = document.querySelector(".reset button")
// let deleteLast = document.querySelector(".delete button")
//
// let errorCharacter = 1;
// let errorDot = 1;
//
// for (let btn of numberTable) {
//     btn.addEventListener("click", function () {
//         if (screen.textContent === "ERR") {
//             screen.textContent = ""
//             screen.textContent = screen.textContent + btn.textContent
//             errorCharacter = 1;
//         } else {
//             screen.textContent = screen.textContent + btn.textContent
//             errorCharacter = 1;
//         }
//     })
// }
//
// dotCharacter.addEventListener("click", function () {
//     errorDot++
//     if (screen.textContent === "") {
//         screen.textContent = "ERR"
//     } else if (errorDot % 2 === 0) {
//         screen.textContent = screen.textContent + dotCharacter.textContent
//     } else {
//         screen.textContent = "ERR"
//         errorDot = 1;
//     }
// })
//
// for (let btn of characterTable) {
//     btn.addEventListener("click", function () {
//         errorCharacter++
//         if (errorCharacter % 2 === 0) {
//             screen.textContent = screen.textContent + btn.textContent
//             errorDot = 1;
//         } else {
//             screen.textContent = "ERR"
//             errorCharacter = 1;
//         }
//     })
// }
//
// reset.addEventListener("click", function () {
//     screen.textContent = ""
//     errorDot = 1;
//     errorCharacter = 1;
// })
//
// deleteLast.addEventListener("click", function () {
//     screen.textContent = screen.textContent.substring(0, screen.textContent.length - 1)
// })
//
// egal.addEventListener("click", function () {
//     egalResult()
// })
//
// function egalResult() {
//     // try {
//     //     eval(screen.textContent)
//     // } catch (error) {
//     //     screen.textContent = "ERR"
//     // }
//
//     if (screen.textContent === "" || screen.textContent === "ERR") {
//         screen.textContent = "ERR"
//     } else {
//         screen.textContent = eval(screen.textContent).toFixed(2)
//         errorCharacter = 1;
//     }
// }

/*********************************************************************************************************************/
/*                                                      V2                                                           */
/*********************************************************************************************************************/

let screen = document.querySelector(".screen-result p")

let numberTable = document.querySelectorAll(".number button")
let characterTable = document.querySelectorAll(".character button")
let dotCharacter = document.querySelector(".dot button")
let egal = document.querySelector(".button-l button")
let reset = document.querySelector(".reset button")
let deleteLast = document.querySelector(".delete button")

let errorCharacter = 1;
let errorDot = 1;

dotCharacter.addEventListener("click", function () {
    errorDot++
    if (screen.textContent === "") {
        screen.textContent = ""
        errorDot = 1;
    } else if (errorDot % 2 === 0) {
        screen.textContent = screen.textContent + dotCharacter.textContent
    }
    errorDot = 0;
})

for (let btn of characterTable) {
    btn.addEventListener("click", function () {
        errorCharacter++
        if (errorCharacter % 2 === 0) {
            screen.textContent = screen.textContent + btn.textContent
        }
        errorCharacter = 0;
    })
}

for (let btn of numberTable) {
    btn.addEventListener("click", function () {
        if (screen.textContent === "ERR") {
            screen.textContent = ""
            screen.textContent = screen.textContent + btn.textContent
            errorCharacter = 1;
            errorDot = 1;
        } else {
            screen.textContent = screen.textContent + btn.textContent
            errorCharacter = 1;
            errorDot = 1;
        }
    })
}

reset.addEventListener("click", function () {
    screen.textContent = ""
    errorDot = 1;
    errorCharacter = 1;
})

deleteLast.addEventListener("click", function () {
    if (screen.textContent.charAt(screen.textContent.length - 1) === "+" ||
        screen.textContent.charAt(screen.textContent.length - 1) === "-" ||
        screen.textContent.charAt(screen.textContent.length - 1) === "*" ||
        screen.textContent.charAt(screen.textContent.length - 1) === "/") {
        errorCharacter = 1;
    }
    screen.textContent = screen.textContent.substring(0, screen.textContent.length - 1)
})

egal.addEventListener("click", function () {
    egalResult()
})

function egalResult() {
    try {
        eval(screen.textContent)
    } catch (error) {
        screen.textContent = "ERR"
    }

    if (screen.textContent === "" || screen.textContent === "ERR") {
        screen.textContent = "ERR"
    } else {
        screen.textContent = eval(screen.textContent).toFixed(2)
        errorCharacter = 1;
        errorDot = 0;
    }
}


/*********************************************************************************************************************/
/*                                                      V3                                                           */
/*********************************************************************************************************************/